const express = require("express");
const app = express();
const port = 3000;

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const Users = [
  {
    name: "Jesse Garcia",
    id: "jesse",
    role: "Developer",
    groups: [
      {
        name: "Company Permissions",
        icon: "fas fa-users",
        permissions: [
          {
            name: "CanDoThing",
            value: false,
          },
          {
            name: "CanDoThingTwo",
            value: true,
          },
          {
            name: "CanDoThingThree",
            value: false,
          },
        ],
      },
      {
        name: "Buyer Permissions",
        icon: "fas fa-cart-plus",
        permissions: [
          {
            name: "CanDoThing",
            value: false,
          },
          {
            name: "CanDoThingTwo",
            value: true,
          },
          {
            name: "CanDoThingThree",
            value: false,
          },
        ],
      },
    ],
  },
  {
    name: "Chris Hudson",
    id: "chris",
    role: "VP",
    groups: [
      {
        name: "Company Permissions",
        icon: "fas fa-users",
        permissions: [
          {
            name: "CanDoThing",
            value: false,
          },
          {
            name: "CanDoThingTwo",
            value: true,
          },
          {
            name: "CanDoThingThree",
            value: false,
          },
        ],
      },
      {
        name: "Buyer Permissions",
        icon: "fas fa-cart-plus",
        permissions: [
          {
            name: "CanDoThing",
            value: false,
          },
          {
            name: "CanDoThingTwo",
            value: true,
          },
          {
            name: "CanDoThingThree",
            value: false,
          },
        ],
      },
    ],
  },
];

app.get("/users", (req, res) => {
  res.json(Users);
});

app.post("/user/update", jsonParser, (req, res) => {
  const user = req.body;
  Users[Users.map((user) => user.id).indexOf(user.id)] = user;
  res.json(Users);
});

app.listen(port, () => {
  console.log(`listening at http://localhost:3000`);
});
