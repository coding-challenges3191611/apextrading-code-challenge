import React, { useState } from 'react';
import { Button } from 'react-bootstrap';

const buttonStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  textAlign: 'center',
  margin: '0.5em',
  background: 'transparent',
  color: 'black',
  border: '1px solid black',
  outline: 'none',
  minWidth: '12em',
};

function PermissionButton({ permission, onClick }) {
  return (
    <Button onClick={onClick} style={buttonStyle}>
      <span>{permission.name}</span>
      {permission.value && (
        <i className="fas fa-check" style={{ color: 'green' }} />
      )}
      {!permission.value && (
        <i className="fas fa-times-circle" style={{ color: 'red' }} />
      )}
    </Button>
  );
}

export default PermissionButton;
